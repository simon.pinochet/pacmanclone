// -----------------
// Pacman Controller
// -----------------

var keyLog = 'd';
var tutorial_num = 1;

class PacmonController {
    constructor(model, view) {
        // Save model and view
        this.model = model;
        this.view = view;

        // Update pacmon
        let updatePacmon = function(intervalId) {
            // Update model
            let update = model.updatePacmon(keyLog);

            // Update view
            if (update[4] == 1) { 
                view.resetBoard(model.nextLevel());
                clearInterval(intervalId);
                play();
            } else if (update[4] == 2) {
                model.loseLife();
                if (update[3] == 0) {
                    clearInterval(intervalId);
                    model = new PacmonModel();
                    view = new PacmonView();
                    view.renderMenu();

                    game_theme.pause();
                    fright_theme.pause();
                } else {
                    game_theme.pause();
                    fright_theme.pause();
                    view.resetCharacters();
                    clearInterval(intervalId);
                    keepPlaying();
                }
            }
            else if (update[4] != 2) { view.updatePacmon(update); }

            if (update[4] == 4) {
                game_theme.pause();
                fright_theme.play();
                setTimeout(function() {
                    fright_theme.pause();
                    game_theme.play();
                }.bind(game_theme), update[5])
            }

            return update[4];
        }

        // Update ghost
        let updateGhosts = function() {
            view.updateGhost(model.updateBlinky(), "blinky", "red");
            view.updateGhost(model.updatePinky(), "pinky", "pink");
            view.updateGhost(model.updateInky(), "inky", "cyan");
            view.updateGhost(model.updateClyde(), "clyde", "orange");
        }

        // Keep playing
        let keepPlaying = function() {
            intro_theme.play();

            setTimeout(function() {
                view.renderCharacters();
                model.resumeTimer();
                game_theme.play();

                let p_status = 0;
                let frame = 0;
                var intervalID = setInterval( function() {
                    // Awaken ghosts
                    if (frame == 60) { model.resetPinky(); }
                    else if (frame == 120) { model.resetInky(); }
                    else if (frame == 180) { model.resetClyde(); }

                    // Update characters
                    if (p_status != 3) { p_status = updatePacmon(intervalID); }
                    else { p_status = 0; }

                    // See if updating ghosts is needed
                    if (p_status != 1 && p_status != 2) {
                        updateGhosts();
                    }

                    frame++;
                }, 16)
            }, 5000);
        }

        // Play game
        let play = function() {
            intro_theme.play();

            setTimeout(function() {
                view.renderCharacters();
                model.scatterTimer();
                game_theme.play();

                let p_status = 0;
                var intervalID = setInterval( function() {
                    // Update characters
                    if (p_status != 3) { p_status = updatePacmon(intervalID); }
                    else { p_status = 0; }

                    // See if updating ghosts is needed
                    if (p_status != 1 && p_status != 2) {
                        updateGhosts();
                    }
                }, 16)
            }, 5000);
        }

        // Tutorial 1
        let tutorial_1h = function() {
            alert("Press d to move right, then press ok!");
            keyLog = 'd';
            tutorial_num = 2;
        }

        // Tutorial 2
        let tutorial_2h = function() {
            alert("Press w to move up, then press ok!");
            keyLog = 'w';
            tutorial_num =  3;
        }

        // Tutorial 3
        let tutorial_3h = function() {
            alert("Press a to move left, then press ok!");
            keyLog = 'a';
            tutorial_num =  4;
        }

        // Tutorial 4
        let tutorial_4h = function() {
            alert("Press s to move down, then press ok!");
            keyLog = 's';
            tutorial_num =  5;
            alert("Try to reach the Power Pellet (the big circles)!");
        }

        // Tutorial 5
        let tutorial_5h = function() {
            alert("When you collect a power pellet you can eat ghosts while they are blue for extra points!");
            tutorial_num =  6;
        }

        // Tutorial 6
        let tutorial_6h = function() {
            alert("The green square will give you bonus points, but will only be there for am limited time!");
            tutorial_num =  7;
        }

        // Update pacmon
        let updatePacmonTutorial = function(intervalId) {
            // Check for tutorial
            if (tutorial_num == 1) {
                tutorial_1h();
            }

            // Update model
            let update = model.updatePacmon(keyLog);

            // Update view
            if (update[4] == 1) { 
                view.resetBoard(model.nextLevel());
                clearInterval(intervalId);
                play();
            } else if (update[4] == 2) {
                model.loseLife();
                if (update[3] == 0) {
                    clearInterval(intervalId);
                    model = new PacmonModel();
                    view = new PacmonView();
                    view.renderMenu();
                } else {
                    game_theme.pause();
                    view.resetCharacters();
                    clearInterval(intervalId);
                    keepPlaying();
                }
            }
            else if (update[4] != 2) { view.updatePacmon(update); }

            if (update[4] == 4) {
                game_theme.pause();
                fright_theme.play();
                setTimeout(function() {
                    fright_theme.pause();
                    game_theme.play();
                }.bind(game_theme), update[5])
            }

            // Check for tutorials
            if (tutorial_num == 2) {
                if (update[0] == 21 && update[1] == 23) {
                    tutorial_2h();
                }
            } else if (tutorial_num == 3) {
                if (update[0] == 21 && update[1] == 20) {
                    tutorial_3h();
                }
            } else if (tutorial_num == 4) {
                if (update[0] == 15 && update[1] == 20) {
                    tutorial_4h();
                }
            } else if (tutorial_num == 5) {
                if (update[4] == 4) {
                    tutorial_5h();
                }
            }

            if (update[7] == 1 && tutorial_num == 6) {
                tutorial_6h();
            }

            return update[4];
        }

        // Play tutorial
        let playTutorial = function() {
            keyLog = '';
            intro_theme.play();

            setTimeout(function() {
                game_theme.play();
                view.renderCharacters();
                model.scatterTimer();

                let p_status = 1;

                var intervalID = setInterval( function() {
                        // Update characters
                        if (p_status != 3) { p_status = updatePacmonTutorial(intervalID); }
                        else { p_status = 0; }

                        // See if updating ghosts is needed
                        if (p_status != 1 && p_status != 2) {
                            updateGhosts();
                        }
                }, 16)
            }, 5000);
        }

        // Create event listener
        let handleClick = function(id) {
            if (id == "1p_button") { view.renderBoard(model.getBoard()); play(); }
            else if (id == "lang_button") { view.createLanguageSelect(); }
            else if (id == "eng_button") { active_lang = "eng"; view.createMenuButtons(); }
            else if (id == "swe_button") { active_lang = "swe"; view.createMenuButtons(); }
            else if (id == "help_button") { view.renderBoard(model.getBoard()); playTutorial(); }
            else if (id == "back_button") { view.renderMenu(); }
            else { console.log(id); }
        }

        // Set up event listeners for clicks
        document.addEventListener('click', function(e) {
            e = e || window.event;
            var target = e.target || e.srcElement,
                text = target.textContent || target.innerText;
            handleClick(target.id);
        }, false);

        // Set event listener for key press
        document.addEventListener('keypress', function(e) {
            keyLog = String.fromCharCode(e.keyCode);
        });

        // Render menu
        this.view.renderMenu();

        // Load songs
        var intro_theme = document.createElement("audio");
        intro_theme.setAttribute("src", "./Assets/Audio/intro_theme.mp3");
        intro_theme.loop = false;

        var game_theme = document.createElement("audio");
        game_theme.setAttribute("src", "./Assets/Audio/game_theme.mp3");
        game_theme.loop = true;

        var fright_theme = document.createElement("audio");
        fright_theme.setAttribute("src", "./Assets/Audio/fright_theme.mp3");
        fright_theme.loop = true;
    }

}