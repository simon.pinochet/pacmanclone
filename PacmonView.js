// ------------
// Dictionaries
// ------------

let eng = {
    "one_player": "1 Player",
    "two_player": "2 Players",
    "language": "./Assets/lang2.png",
    "english": "English",
    "swedish": "Svenska",
    "help": "./Assets/help2.png",
    "high_score": "High Score",
    "lives": "Lives"
}

let swe = {
    "one_player": "1 Spelare",
    "two_player": "2 Spelare",
    "language": "./Assets/lang_swe2.png",
    "english": "English",
    "swedish": "Svenska",
    "help": "./Assets/help_swe2.png",
    "high_score": "Rekord",
    "lives": "Liv"
}

// -----------
// Pacman View
// -----------

let menu_html = "";

let r_height = (85 / 31);
let r_width = (76.774193548387 / 28);
let active_lang = "eng";

let pacmon_speed = 0;
let pacmon_frame = 0;

class PacmonView {
    constructor() {
        // Create game screen
        let screen = document.createElement("screen");
        screen.id = "screen";
        screen.innerHTML = "";

        // Add screen to body
        let body = document.getElementsByTagName("body")[0];
        body.innerHTML = "";
        body.append(screen);
    }

    // --------------
    // Menu Rendering
    // --------------

    // Render main menu
    renderMenu() {
        // Check if tutorial is active
        let tutorial = document.getElementById("tutorial");
        if (tutorial != null) { tutorial.remove(); }

        // Choose language
        let dict = eng;
        if (active_lang == "swe") { dict = swe; }

        // Logo and score
        let high_score = document.createElement("p");
        high_score.innerHTML = dict.high_score;
        high_score.className = "high_score";
        high_score.id = "high_score";

        let high_score_number = document.createElement("p");
        high_score_number.innerHTML = 0;
        high_score_number.className = "high_score_number";

        let logo = document.createElement("img");
        logo.src = "./Assets/new_logo2.png";
        logo.className = "logo";
        logo.id = "logo";

        // Buttons
        let menu_buttons = document.createElement("div");
        menu_buttons.className = "menu_buttons";
        menu_buttons.id = "menu_buttons";

        // Append elements to body and save
        let screen = document.getElementById("screen");

        screen.appendChild(high_score);
        screen.appendChild(high_score_number);
        screen.appendChild(logo);
        screen.appendChild(menu_buttons);

        // Create buttons
        this.createMenuButtons();
    }

    createMenuButtons() {
        // Choose language
        let dict = eng;
        if (active_lang == "swe") { dict = swe; }

        // Get menu buttons and clear it
        let menu_buttons = document.getElementById("menu_buttons");
        menu_buttons.innerHTML = "";

        // Create buttons
        let one_player_img = document.createElement("img");
        one_player_img.src = "./Assets/1_player2.png";
        one_player_img.id = "1p_button";
        one_player_img.className = "menu_button";
        menu_buttons.appendChild(one_player_img);

        let lang_img = document.createElement("img");
        lang_img.src = dict.language;
        lang_img.id = "lang_button";
        lang_img.className = "menu_button";
        menu_buttons.appendChild(lang_img);

        let help_img = document.createElement("img");
        help_img.src = dict.help;
        help_img.id = "help_button";
        help_img.className = "menu_button";
        menu_buttons.appendChild(help_img);

        // Translate high score
        let high_score = document.getElementById("high_score");
        high_score.innerHTML = dict.high_score;
    }

    createLanguageSelect() {
        // Get menu buttons and clear it
        let menu_buttons = document.getElementById("menu_buttons");
        menu_buttons.innerHTML = "";

        // Create buttons
        let eng_img = document.createElement("img");
        eng_img.src = "./Assets/english2.png";
        eng_img.id = "eng_button";
        eng_img.className = "menu_button";
        menu_buttons.appendChild(eng_img);

        let swe_img = document.createElement("img");
        swe_img.src = "./Assets/swedish2.png";
        swe_img.id = "swe_button";
        swe_img.className = "menu_button";
        menu_buttons.appendChild(swe_img);
    }

    // ---------------
    // Board Rendering
    // ---------------

    renderUI(screen) {
        // Choose language
        let dict = eng;
        if (active_lang == "swe") { dict = swe; }

        // Player 1 elements
        let one_UP = document.createElement("p");
        one_UP.className = "one_UP";
        one_UP.innerHTML = "1UP";
        screen.appendChild(one_UP);

        let score_1p = document.createElement("p");
        score_1p.className = "score_1p";
        score_1p.id = "score_1p";
        score_1p.innerHTML = 0;
        screen.appendChild(score_1p);

        // Player 2 elements
        let two_UP = document.createElement("p");
        two_UP.className = "two_UP";
        two_UP.innerHTML = "2UP";
        screen.appendChild(two_UP);

        let score_2p = document.createElement("p");
        score_2p.className = "score_2p";
        score_2p.id = "score_2p";
        score_2p.innerHTML = 0;
        screen.appendChild(score_2p);

        // Lives
        let lives = document.createElement("p");
        lives.className = "lives";
        lives.id = "lives";
        lives.innerHTML = dict.lives + ": 3";
        screen.appendChild(lives);
    }

    // Add object to board
    addObject(board, obj, r, c, size, color) {
        obj.style.position = "absolute";
        obj.style.height = (r_height * size) + "vh";
        obj.style.width = (r_width * size) + "vh";
        obj.style.top = (r * r_height) + (r_height / 2) + "vh";
        obj.style.left = (c * r_width) + (r_width / 2) + "vh";
        obj.style.transform = "translate(-50%, -50%)";
        obj.style.backgroundColor = color;
        board.appendChild(obj);
    };

    // Render initial board
    renderBoard(board) {
        // Get screen and delete buttons
        let screen = document.getElementById("screen");
        
        let logo = document.getElementById("logo");
        let menu = document.getElementById("menu_buttons");

        if (logo != null) { logo.remove(); }
        if (menu != null) { menu.remove(); }

        // Create top elements
        this.renderUI(screen);

        // Create board space
        let board_screen = document.createElement("board");
        board_screen.className ="board";
        board_screen.id = "board";


        // Create bonus element
        let bonus = document.createElement("bonus");
        bonus.id = "bonus";
        bonus.style.visibility = "hidden";
        this.addObject(board_screen, bonus, 17, 13.5, 0.75, "green");

        // Populate board
        let r = 0;
        let c = 0;
        let intervalId = setInterval( function() {
            if (board[r][c] == 'd') {
                let dot = document.createElement("dot");
                dot.id = "dot" + r + "_" + c;
                this.addObject(board_screen, dot, r, c, 0.15, "blue")
            } else if (board[r][c] == 'b') {
                let pellet = document.createElement("pellet");
                pellet.id = "dot" + r + "_" + c;
                pellet.style.borderRadius = "50%";
                this.addObject(board_screen, pellet, r, c, 0.5, "blue")
            }

            if (c == board[r].length) {
                c = 0;
                r++;
            } else {
                ++c;
            }
            
            if (r == board.length) {
                clearInterval(intervalId);
            }
        }.bind(this), 4);

        // Add board to screen
        screen.appendChild(board_screen);
    }

    // Remove characters
    resetCharacters() {
        document.getElementById("pacmon").remove();
        document.getElementById("blinky").remove();
        document.getElementById("pinky").remove();
        document.getElementById("inky").remove();
        document.getElementById("clyde").remove();
    }

    resetBoard(board) {
        // Remove characters
        this.resetCharacters();

        // Create board space
        let board_screen = document.getElementById("board");

        // Add object to board
        let addObject = function(board, obj, r, c, size, color) {
            obj.style.position = "absolute";
            obj.style.height = (r_height * size) + "vh";
            obj.style.width = (r_width * size) + "vh";
            obj.style.top = (r * r_height) + (r_height / 2) + "vh";
            obj.style.left = (c * r_width) + (r_width / 2) + "vh";
            obj.style.transform = "translate(-50%, -50%)";
            obj.style.backgroundColor = color;
            board.appendChild(obj);
        };

        // Populate board
        let r = 0;
        let c = 0;
        let intervalId = setInterval( function() {
            if (board[r][c] == 'd') {
                let dot = document.createElement("dot");
                dot.id = "dot" + r + "_" + c;
                addObject(board_screen, dot, r, c, 0.15, "blue")
            } else if (board[r][c] == 'b') {
                let pellet = document.createElement("pellet");
                pellet.id = "dot" + r + "_" + c;
                pellet.style.borderRadius = "50%";
                addObject(board_screen, pellet, r, c, 0.5, "blue")
            }

            if (c == board[r].length) {
                c = 0;
                r++;
            } else {
                ++c;
            }
            
            if (r == board.length) {
                clearInterval(intervalId);
            }
        }, 4);
    }

    // --------------------
    // Characters Rendering
    // --------------------

    // Add character to board
    addCharacter(board, obj, r, c, url) {
        obj.style.position = "absolute";
        obj.style.height = r_height + "vh";
        obj.style.width = r_width + "vh";
        obj.style.top = (r * r_height) + (r_height / 2) + "vh";
        obj.style.left = (c * r_width) + (r_width / 2) + "vh";
        obj.style.transform = "translate(-50%, -50%)";
        obj.src = url;
        board.appendChild(obj);
    }

    // Render characters in their initial position
    renderCharacters() {
        // Get board screen
        let board_screen = document.getElementById("board");

        // Render pacmon
        let pacmon = document.createElement("img");
        pacmon.id = "pacmon";
        this.addCharacter(board_screen, pacmon, 23, 13.5, "./Assets/Pacmon/frame0_1_02.png");

        // Render blinky
        var blinky = document.createElement("img");
        blinky.id = "blinky";
        this.addCharacter(board_screen, blinky, 11, 13.5, "./Assets/Ghosts/blinky0.png");

        // Render pinky
        var pinky = document.createElement("img");
        pinky.id = "pinky";
        this.addCharacter(board_screen, pinky, 14, 13.5, "./Assets/Ghosts/pinky0.png");

        // Render inky
        var inky = document.createElement("img");
        inky.id = "inky";
        this.addCharacter(board_screen, inky, 14, 11.5, "./Assets/Ghosts/inky0.png");

        // Render clyde
        var clyde = document.createElement("img");
        clyde.id = "clyde";
        this.addCharacter(board_screen, clyde, 14, 15.5, "./Assets/Ghosts/clyde0.png");
    }

    // Update pacmon's position
    updatePacmon(update) {
        // Update pacmon's position
        let pacmon = document.getElementById("pacmon");
        pacmon.style.top = (update[1] * r_height) + (r_height / 2) + "vh";
        pacmon.style.left = (update[0] * r_width) + (r_width / 2) + "vh";

        // Update pacmon's sprite
        if (pacmon_speed == 1) {
            pacmon_speed = 0;
            ++pacmon_frame;
            if (pacmon_frame == 4) { pacmon_frame = 0; };
            if (!(update[8] == 0 && update[9] == 0)) { pacmon.src = "./Assets/Pacmon/frame" + pacmon_frame + "_" + Math.sign(update[8]) + "_" + Math.sign(update[9]) + "2.png"; }
        } else {
            ++pacmon_speed;
        }

        // Check for bonus items
        if (update[7] != 0) {
            document.getElementById("bonus").style.visibility = "visible";
        } else {
            let score_1p = document.getElementById("score_1p");
            score_1p.innerHTML = update[2];
            document.getElementById("bonus").style.visibility = "hidden";
        }

        // Update score
        if (update[4] == 3 || update[4] == 4) {
            document.getElementById("dot" + update[1] + "_" + update[0]).remove();
        }

        // Update lives
        // Choose language
        let dict = eng;
        if (active_lang == "swe") { dict = swe; }

        let lives = document.getElementById("lives");
        lives.innerHTML = dict.lives + ": " + update[3];
    }

    // Update ghost's position
    updateGhost(update, name, color) {
        // Update ghost's position
        let ghost = document.getElementById(name);
        ghost.style.top = (update[1] * r_height) + (r_height / 2) + "vh";
        ghost.style.left = (update[0] * r_width) + (r_width / 2) + "vh";

        // Update sprite
        if (update[2] == 0) {
            ghost.style.height = (r_height * 0.4) + "vh";
            ghost.style.width = (r_width * 0.4) + "vh";
            ghost.src = "./Assets/Ghosts/dead" + (pacmon_frame % 2) + ".png";
        } else if (update[2] == 1) {
            ghost.style.height = r_height + "vh";
            ghost.style.width = r_width + "vh";
            ghost.src = "./Assets/Ghosts/" + name + (pacmon_frame % 2) + ".png";
        } else if (update[2] == 2) {
            ghost.style.height = (r_height * 0.8) + "vh";
            ghost.style.width = (r_width * 0.8) + "vh";
            ghost.src = "./Assets/Ghosts/fright" + (pacmon_frame % 2) + ".png";
        }
    }
}