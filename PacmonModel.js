// http://stackoverflow.com/questions/7279567/how-do-i-pause-a-window-setinterval-in-javascript
function Timer(callback, delay) {
    var timerId, start, remaining = delay;

    this.pause = function() {
        window.clearTimeout(timerId);
        remaining -= new Date() - start;
    };

    this.resume = function() {
        start = new Date();
        window.clearTimeout(timerId);
        timerId = window.setTimeout(callback, remaining);
    };

    this.delete = function() {
        window.clearTimeout(timerId);
    }

    this.resume();
}

// ------------
// Pacmon Model
// ------------

class PacmonModel {
    constructor() {
        // Initialize maps
        this.map = this.createBoard();
        this.binary_map = this.createBinaryBoard();

        // Initialize variables
        this.mode = 1;  // 0 for chase, 1 for scatter, and 2 for fright
        this.total_dots = 244;
        this.score = 0;
        this.ghosts_eaten = 0;
        this.lives = 3;
        this.level = 1;

        // Bonus points
        this.bonus_points = 100;
        this.bonus_flag = 0;
        this.bonus_timer;

        // Pacmon's coordinates
        this.pacmon = {
            speed: (1 / 6) * 0.8,
            x: 13.5,
            y: 23.0,
            vx: (1 / 6) * 0.8,
            vy: 0.0,
            status: 0,
            flag: 0
        }

        this.ghost_speed = (1 / 6) * 0.75;
        this.tunnel_speed = (1 / 6) * 0.4;

        // Fright data
        this.fright_speed = (1 / 6) * 0.9;
        this.fright_ghost_speed = (1 / 6) * 0.5;
        this.fright_time = 6000;
        this.fright_flashes = 5;

        // Blinky's coordinates
        this.blinky = {
            x: 13.5,
            y: 11,
            vx: this.ghost_speed,
            vy: 0,
            status: 1
        }

        // Pinky's coordinates
        this.pinky = {
            x: 13.5,
            y: 14,
            vx: this.ghost_speed,
            vy: 0,
            status: 1
        }

        // Inky's coordinates
        this.inky = {
            x: 11.5,
            y: 14,
            vx: this.ghost_speed,
            vy: 0,
            status: 1,
            flag: 0
        }

        // Clyde's coordinates
        this.clyde = {
            x: 15.5,
            y: 14,
            vx: this.ghost_speed,
            vy: 0,
            status: 1,
            flag: 0
        }

        // Elroy Mode
        this.elroy1_speed = (1 / 6) * 0.8;
        this.elroy1_dots = 20;
        this.elroy2_speed = (1 / 6) * 0.85;
        this.elroy2_speed = 10;
    }

    // Create pacmon's board
    createBoard() {
        /*
        * w = Wall
        * e = Empty
        * d = Dot
        * b = Power Pellet
        */

        var map = new Array();
        map[0] = new Array('w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w');
        map[1] = new Array('w', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'w');
        map[2] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'd', 'w');
        map[3] = new Array('w', 'b', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'b', 'w');
        map[4] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'd', 'w');
        map[5] = new Array('w', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'w');
        map[6] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'd', 'w');
        map[7] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'd', 'w');
        map[8] = new Array('w', 'd', 'd', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'd', 'd', 'w');
        map[9] = new Array('w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'e', 'w', 'w', 'e', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w');
        map[10] = new Array('e', 'e', 'e', 'e', 'e', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'e', 'w', 'w', 'e', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'e', 'e', 'e', 'e', 'e');
        map[11] = new Array('e', 'e', 'e', 'e', 'e', 'w', 'd', 'w', 'w', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'w', 'w', 'd', 'w', 'e', 'e', 'e', 'e', 'e');
        map[12] = new Array('e', 'e', 'e', 'e', 'e', 'w', 'd', 'w', 'w', 'e', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'e', 'w', 'w', 'd', 'w', 'e', 'e', 'e', 'e', 'e');
        map[13] = new Array('w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'e', 'w', 'e', 'e', 'e', 'e', 'e', 'e', 'w', 'e', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w');
        map[14] = new Array('e', 'e', 'e', 'e', 'e', 'e', 'd', 'e', 'e', 'e', 'w', 'e', 'e', 'e', 'e', 'e', 'e', 'w', 'e', 'e', 'e', 'd', 'e', 'e', 'e', 'e', 'e', 'e');
        map[15] = new Array('w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'e', 'w', 'e', 'e', 'e', 'e', 'e', 'e', 'w', 'e', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w');
        map[16] = new Array('e', 'e', 'e', 'e', 'e', 'w', 'd', 'w', 'w', 'e', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'e', 'w', 'w', 'd', 'w', 'e', 'e', 'e', 'e', 'e');
        map[17] = new Array('e', 'e', 'e', 'e', 'e', 'w', 'd', 'w', 'w', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'w', 'w', 'd', 'w', 'e', 'e', 'e', 'e', 'e');
        map[18] = new Array('e', 'e', 'e', 'e', 'e', 'w', 'd', 'w', 'w', 'e', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'e', 'w', 'w', 'd', 'w', 'e', 'e', 'e', 'e', 'e');
        map[19] = new Array('w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'e', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'e', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w');
        map[20] = new Array('w', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'w');
        map[21] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'd', 'w');
        map[22] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'd', 'w');
        map[23] = new Array('w', 'b', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'e', 'e', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'b', 'w');
        map[24] = new Array('w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w');
        map[25] = new Array('w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w');
        map[26] = new Array('w', 'd', 'd', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'w', 'w', 'd', 'd', 'd', 'd', 'd', 'd', 'w');
        map[27] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w');
        map[28] = new Array('w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w', 'w', 'd', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'd', 'w');
        map[29] = new Array('w', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'w');
        map[30] = new Array('w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w');
        return map;
    }

    // Create binary maze for the algorithm
    createBinaryBoard() {
        let binary_board = new Array();
        for (let r = 0; r < this.map.length; ++r) {
            binary_board[r] = new Array();
            for (let c = 0; c < this.map[r].length; ++c) {
                if (this.map[r][c] == 'w') {
                    binary_board[r][c] = 0;
                } else {
                    binary_board[r][c] = 1;
                }
            }
        }
        return binary_board;
    }

    // Return board for the view
    getBoard() { return this.map; }

    // ----------------
    // General Movement
    // ----------------

    // Round coordinates
    roundCoordinates(x, y, speed) {
        if (x % Math.trunc(x) >= (1 - (speed / 2)) || x % Math.trunc(x) < (speed / 2)) { x = Math.trunc(x + speed); }
        if (y % Math.trunc(y) >= (1 - (speed / 2)) || y % Math.trunc(y) < (speed / 2)) { y = Math.trunc(y + speed); }
        if (y <= 1) { y = 1; }
        if (y != 14 && x <= 1) { x = 1; }

        // Check if blinky hits the tunnel
        if (y == 14 && x <= 0) { x = this.map[0].length - 1; }
        else if (y == 14 && x >= this.map[0].length - 1) { x = 0; }

        return Array(x, y);
    }

    // Check if intersection
    isCentered(point) {
        return (point % Math.trunc(point) == 0);
    }

    // Check collision
    collision(x, y) {
        return Math.trunc(this.pacmon.x) == Math.trunc(x) && Math.trunc(this.pacmon.y) == Math.trunc(y);
    }

    // ---------------
    // Pacman Movement
    // ---------------

    // Spawn bonus points item
    spawnBonus() {
        // Make bonus item appear
        this.bonus_flag = 1;

        // Set timeout to make it disappear
        this.bonus_timer = setTimeout(function() {
            this.bonus_flag = 0
        }.bind(this), 6000);
    }

    // Update pacmon's position
    updatePacmon(input) {
        // Board cleared
        if (this.total_dots == 0) {
            // Next round
            return Array(this.pacmon.x, this.pacmon.y, this.score, this.lives, 1);
        }

        // Check if collision
        if (this.pacmon.status == 2) {
            // Lose life
            return Array(this.pacmon.x, this.pacmon.y, this.score, this.lives, 2);
        }
        
        // Check for bonus points
        if (this.bonus_flag == 0 && (this.total_dots == 174 || this.total_dots == 74)) {
            this.spawnBonus();
        }

        // Create local variables
        let x = this.pacmon.x;
        let y = this.pacmon.y;
        let vx = this.pacmon.vx;
        let vy = this.pacmon.vy;
        let speed = this.pacmon.speed;

        // Check if fright mode
        if (this.mode == 2) { speed = this.fright_speed; }

        // Compute movement vector
        if (input == 'w' && this.isCentered(x) && this.map[Math.trunc(y - 0.1)][Math.trunc(x)] != 'w') {
            vx = 0;
            vy = -1 * speed;
        } else if (input == 'd' && this.isCentered(y) && this.map[Math.trunc(y)][Math.trunc(x) + 1] != 'w') {
            vx = speed;
            vy = 0;
        } else if (input == 's' && this.isCentered(x) && this.map[Math.trunc(y) + 1][Math.trunc(x)] != 'w') {
            vx = 0;
            vy = speed;
        } else if (input == 'a' && this.isCentered(y) && this.map[Math.trunc(y)][Math.trunc(x - 0.1)] != 'w') {
            vx = -1 * speed;
            vy = 0;
        } else if (this.isCentered(x) && this.isCentered(y) && this.map[Math.trunc(y + Math.sign(vy))][Math.trunc(x + Math.sign(vx))] == 'w') {
            vx = 0;
            vy = 0;
        }

        // Compute pacman's x and y coordinate
        x += vx;
        y += vy;

        // Round if necessary
        let coordinates = this.roundCoordinates(x, y, speed);
        x = coordinates[0];
        y = coordinates[1];

        // Update score
        this.pacmon.status = 0;
        if (this.isCentered(x) && this.isCentered(y)) {
            if (this.map[y][x] == 'd') {
                this.map[y][x] = 'e';
                this.score += 10;
                this.total_dots -= 1;
                this.pacmon.status = 3;
            } else if (this.map[y][x] == 'b') {
                this.map[y][x] = 'e';
                this.score += 50;
                this.total_dots -= 1;
                this.pacmon.status = 4;

                // Start fright mode
                this.frightMode();
            } else if (this.bonus_flag == 1 && (x == 13 || x == 14) && y == 17) {
                console.log("Hi");
                this.bonus_flag = 0;
                clearTimeout(this.bonus_timer);
                this.score += this.bonus_points;
            }
        }

        // Check extra life
        if (this.score >= 10000 && this.pacmon.flag == 0) {
            this.pacmon.flag = 1;
            this.lives += 1;
        }

        // Update global coordinates
        this.pacmon.x = x;
        this.pacmon.y = y;
        this.pacmon.vx = vx;
        this.pacmon.vy = vy;

        // Return pacmon's x and y coordinates, score. lives, flag, fright time and fright flashes
        return Array(this.pacmon.x, this.pacmon.y, this.score, this.lives, this.pacmon.status, this.fright_time, this.fright_flashes, this.bonus_flag, this.pacmon.vx, this.pacmon.vy);
    }

    // ---------------
    // Ghosts Movement
    // ---------------

    // Reverse ghosts movement
    reverseGhosts() {
        // Reverse Blinky
        if (this.blinky.status != 2) {
            this.blinky.vx *= -1;
            this.blinky.vy *= -1;
        }

        // Reverse Pinky
        if (this.pinky.status != 2) {
            this.pinky.vx *= -1;
            this.pinky.vy *= -1;
        }

        // Reverse Inky
        if (this.inky.status != 2) {
            this.inky.vx *= -1;
            this.inky.vy *= -1;
        }

        // Reverse Clyde
        if (this.clyde.status != 2) {
            this.clyde.vx *= -1;
            this.clyde.vy *= -1;
        }
    }

    // Set scatter and chase times for level 1
    createTimer(t1, t2, t3, t4, t5, t6, t7) {
        // First scatter
        var timer_1 = new Timer(function() {
            // First chase
            this.mode = 0;
            this.reverseGhosts();

            var timer_2 = new Timer(function() {
                // Second scatter
                this.mode = 1;
                this.reverseGhosts();

                var timer_3 = new Timer(function() {
                    // Second chase
                    this.mode = 0;
                    this.reverseGhosts();

                    var timer_4 = new Timer(function() {
                        // Third scatter
                        this.mode = 1;
                        this.reverseGhosts();

                        var timer_5 = new Timer(function() {
                            // Third chase
                            this.mode = 0;
                            this.reverseGhosts();

                            var timer_6 = new Timer(function() {
                                if (t7 > 0) {
                                    // fourth scatter
                                    this.mode = 1;
                                    this.reverseGhosts();

                                    var timer_7 = new Timer(function() {
                                        // Indefinite scatter
                                        this.mode = 0;
                                        this.reverseGhosts();
                                    }.bind(this), t7);

                                    // Set global timer
                                    this.timer = timer_7;
                                } else { this.reverseGhosts(); }
                            }.bind(this), t6);

                            // Set global timer
                            this.timer = timer_6;
                        }.bind(this), t5);

                        // Set global timer
                        this.timer = timer_5;
                    }.bind(this), t4);

                    // Set global timer
                    this.timer = timer_4;
                }.bind(this), t3);

                // Set global timer
                this.timer = timer_3;
            }.bind(this), t2);

            // Set global timer
            this.timer = timer_2;
        }.bind(this), t1);

        // Set global timer
        this.timer = timer_1;
    }

    // Set scatter and chase times depending on the level
    scatterTimer() {
        if (this.level == 1) { this.createTimer(7000, 20000, 7000, 20000, 5000, 20000, 5000); }
        else if (this.level < 5) { this.createTimer(7000, 20000, 7000, 20000, 5000, 1033000, 0); }
        else { this.createTimer(5000, 20000, 5000, 20000, 5000, 1037000, 0); }
    }

    // Enter fright mode
    frightMode() {
        // Pause scatter timer
        if (this.mode != 2) { this.prev_mode = this.mode; }
        else { clearTimeout(this.fright_timer); }
        this.mode = 2;
        this.reverseGhosts();
        this.timer.pause();

        // Set up fright mode timer
        this.fright_timer = setTimeout(function() {
            this.timer.resume();
            this.mode = this.prev_mode;

            // Update ghosts status
            if (this.blinky.status == 2) { this.blinky.status = 1; }
            if (this.pinky.status == 2) { this.pinky.status = 1; }
            if (this.inky.status == 2) { this.inky.status = 1; }
            if (this.clyde.status == 2) { this.clyde.status = 1; }
        }.bind(this), this.fright_time);

        // Update ghosts status
        if (this.blinky.status == 1) { this.blinky.status = 2; }
        if (this.pinky.status == 1) { this.pinky.status = 2; }
        if (this.inky.status == 1) { this.inky.status = 2; }
        if (this.clyde.status == 1) { this.clyde.status = 2; }
    }

    // Check if space is an intersection
    isIntersection(x, y) {
        if (x == 0 || x == 27) { return false; }

        if (this.binary_map[y][x] != 0) {
            let sum = 0
            if (this.binary_map[y + 1][x] != 0) { ++sum; }
            if (this.binary_map[y - 1][x] != 0) { ++sum; }
            if (this.binary_map[y][x + 1] != 0) { ++sum; }
            if (this.binary_map[y][x - 1] != 0) { ++sum; }
            
            if (sum >= 3) { return true; }
        }
        return false;
    }

    // Check if restricted area
    isRestricted(x, y) {
        return (12 <= x || x <= 15) && (y == 23 || y == 11) && (this.mode != 2);
    }

    // Keep straight
    keepStraight(ghost, speed) {
        // Check for walls
        if (this.map[Math.trunc(ghost.y + Math.sign(ghost.vy))][Math.trunc(ghost.x + Math.sign(ghost.vx))] == 'w') {
            // Change direction
            if (ghost.vy == 0 && this.map[Math.trunc(ghost.y - 0.1)][Math.trunc(ghost.x)] != 'w') {
                return Array(0, -1 * speed);
            } else if (ghost.vx == 0 && this.map[Math.trunc(ghost.y)][Math.trunc(ghost.x - 1)] != 'w') {
                return Array(-1 * speed, 0);
            } else if (ghost.vy == 0 && this.map[Math.trunc(ghost.y) + 1][Math.trunc(ghost.x)] != 'w') {
                return Array(0, speed);
            } else if (ghost.vx == 0 && this.map[Math.trunc(ghost.y)][Math.trunc(ghost.x) + 1] != 'w') {
                return Array(speed, 0);
            }
        }

        // No walls
        return Array(ghost.vx, ghost.vy);
    }

    // Compute vector length to target
    computeVector(x, y, target_x, target_y) {
        return Math.sqrt(((x - target_x) * (x - target_x)) + (y - target_y) * (y - target_y));
    }

    // Get fastest way to coordinates
    toTarget(ghost, target, speed) {
        if (this.isCentered(ghost.x) && this.isCentered(ghost.y)) {
            if (this.isIntersection(Math.trunc(ghost.x), Math.trunc(ghost.y))) {
                // Array to store shortest path and it's length
                let path = Array(ghost.vx, ghost.vy, 200);

                // Check up path
                if (Math.sign(ghost.vy) <= 0 && this.map[Math.trunc(ghost.y - 0.1)][Math.trunc(ghost.x)] != 'w' && !this.isRestricted(ghost.x, ghost.y)) {
                    let up_path = this.computeVector(ghost.x, ghost.y - 1, target.x, target.y);
                    if (up_path < path[2]) { path = Array(0, -1 * speed, up_path); }
                }

                // Check left path
                if (Math.sign(ghost.vx) <= 0 && this.map[Math.trunc(ghost.y)][Math.trunc(ghost.x - 0.1)] != 'w') {
                    let left_path = this.computeVector(ghost.x - 1, ghost.y, target.x, target.y);
                    if (left_path < path[2]) { path = Array(-1 * speed, 0, left_path); }
                }

                // Check down path
                if (Math.sign(ghost.vy) >= 0 && this.map[Math.trunc(ghost.y) + 1][Math.trunc(ghost.x)] != 'w') {
                    let down_path = this.computeVector(ghost.x, ghost.y + 1, target.x, target.y);
                    if (down_path < path[2]) { path = Array(0, speed, down_path); }
                }

                // Check right path
                if (Math.sign(ghost.vx) >= 0 && this.map[Math.trunc(ghost.y)][Math.trunc(ghost.x) + 1] != 'w') {
                    let right_path = this.computeVector(ghost.x + 1, ghost.y, target.x, target.y);
                    if (right_path < path[2]) { path = Array(speed, 0, right_path); }
                }

                return Array(path[0], path[1]);
            } else {
                return this.keepStraight(ghost, speed);
            }
        }

        return Array(ghost.vx, ghost.vy);
    }

    // Compute next move
    computeGhostMovement(ghost, target_x, target_y, speed) {
        // Check if ghost is dead
        if (ghost.status == 0) {
            target_x = 13.5;
            target_y = 11;
        } else if (ghost.status == 2) {
            target_x = Math.random() * 27;
            target_y = Math.random() * 30;
            speed = this.fright_ghost_speed;
        }

        // Update speed for tunnel
        if (ghost.y == 14 && (ghost.x < 5 || ghost.x > 22)) { speed = this.tunnel_speed; }
        
        // Compute new vectors according to speed
        ghost.vx = Math.sign(ghost.vx) * speed;
        ghost.vy = Math.sign(ghost.vy) * speed;

        // Compute path and coordinates
        let path = this.toTarget(ghost, {x: target_x, y: target_y}, speed);
        let coordinates = this.roundCoordinates(ghost.x + path[0], ghost.y + path[1], speed);

        // Check collision
        if (this.collision(ghost.x, ghost.y)) {
            if (ghost.status == 1) {
                this.lives -= 1;
                this.pacmon.status = 2;
            } else if (ghost.status == 2) {
                this.ghosts_eaten += 1;
                this.score += Math.pow(2, this.ghosts_eaten) * 100;
                ghost.status = 0;
            }
        }

        // Check if ghost is revived
        if (ghost.status == 0 && Math.trunc(coordinates[0]) == Math.trunc(target_x) && Math.trunc(coordinates[1]) == Math.trunc(target_y)) {
            ghost.status = 1;
        }

        // Return updated variables
        return Array(coordinates[0], coordinates[1], path[0], path[1], ghost.status);
    }

    // Update blinky's position
    updateBlinky() {
        // Compute target
        let target_x = this.pacmon.x;
        let target_y = this.pacmon.y;

        // Check if blinky needs a speed up
        let speed = this.ghost_speed;
        // Check if scatter mode
        if (this.total_dots > this.elroy1_dots && this.mode == 1) {
            target_x = 28;
            target_y = 0;
        } else if (this.total_dots <= this.elroy2_dots) { speed = this.elroy2_speed; }
        else if (this.total_dots <= this.elroy1_dots) { speed = this.elroy1_speed; }

        // Compute movement
        let update = this.computeGhostMovement(this.blinky, target_x, target_y, speed);

        // Update global variables
        this.blinky.x = update[0];
        this.blinky.y = update[1];
        this.blinky.vx = update[2];
        this.blinky.vy = update[3];
        this.blinky.status = update[4];

        // Return coordinates
        return Array(this.blinky.x, this.blinky.y, this.blinky.status);
    }

    // Reset pinky's position
    resetPinky() {
        if (this.total_dots <= 243) {
            this.pinky.y = 11;
            this.pinky_vy = 0;
            this.pinky.vx = -1 * this.ghost_speed;
        }
    }

    // Update pinky's position
    updatePinky() {
        // See if pinky can leave the house
        if (this.total_dots == 243) {
            // Leave house
            this.resetPinky();
        } else if (this.total_dots < 243) {
            // Compute target tile
            let target_x = this.pacmon.x + (Math.sign(this.pacmon.vx) * 4);
            let target_y = this.pacmon.y + (Math.sign(this.pacmon.vy) * 4);

            // Check mode
            if (this.mode == 1) {
                target_x = 0;
                target_y = 0;
            }

            // Glitch in the original pacman
            if (Math.sign(this.pacmon.vy) > 0) {
                target_x -= 4;
            }

            // Compute movement
            let update = this.computeGhostMovement(this.pinky, target_x, target_y, this.ghost_speed);

            // Update global variables
            this.pinky.x = update[0];
            this.pinky.y = update[1];
            this.pinky.vx = update[2];
            this.pinky.vy = update[3];
            this.pinky.status = update[4];
        }

        // Return coordinates
        return Array(this.pinky.x, this.pinky.y, this.pinky.status);
    }

    // Reset inky's position
    resetInky() {
        if (this.inky.flag == 0 && this.total_dots <= 214) {
            this.inky.flag = 1;
            this.inky.x = 13.5;
            this.inky.y = 11;
            this.inky.vy = 0;
            this.inky.vx = this.ghost_speed;
        }
    }

    // Update inky's position
    updateInky() {
        // See if inky can leave the house
        if (this.total_dots == 214) {
            // Leave house
            this.resetInky();
        } else if (this.total_dots < 214) {
            // Compute target tile
            let target_x = this.pacmon.x + (Math.sign(this.pacmon.vx) * 2);
            let target_y = this.pacmon.y + (Math.sign(this.pacmon.vy) * 2);
            target_x = target_x + (target_x - this.blinky.x);
            target_y = target_y + (target_y - this.blinky.y);

            // Check mode
            if (this.mode == 1) {
                target_x = 28;
                target_y = 30;
            }

            // Compute movement
            let update = this.computeGhostMovement(this.inky, target_x, target_y, this.ghost_speed);

            // Update global variables
            this.inky.x = update[0];
            this.inky.y = update[1];
            this.inky.vx = update[2];
            this.inky.vy = update[3];
            this.inky.status = update[4];
        }

        // Return coordinates
        return Array(this.inky.x, this.inky.y, this.inky.status);
    }

    // Reset clyde's position
    resetClyde() {
        if (this.clyde.flag == 0 && this.total_dots <= 213) {
            this.clyde.flag = 1;
            this.clyde.x = 13.5;
            this.clyde.y = 11;
            this.clyde.vy = 0;
            this.clyde.vx = -1 * this.ghost_speed;
        }
    }

    // Update clyde's position
    updateClyde() {
        // See if inky can leave the house
        if (this.total_dots == 160) {
            // Leave house
            this.resetClyde();
        } else if (this.total_dots < 160) {
            // Compute target tile
            let target_x = this.pacmon.x;
            let target_y = this.pacmon.y;

            // Check mode
            if (this.mode == 1 || Math.abs(this.computeVector(this.clyde.x, this.clyde.y, this.pacmon.x, this.pacmon.y)) <= 8) {
                target_x = 0;
                target_y = 30;
            }

            // Compute movement
            let update = this.computeGhostMovement(this.clyde, target_x, target_y, this.ghost_speed);

            // Update global variables
            this.clyde.x = update[0];
            this.clyde.y = update[1];
            this.clyde.vx = update[2];
            this.clyde.vy = update[3];
            this.clyde.status = update[4];
        }

        // Return coordinates
        return Array(this.clyde.x, this.clyde.y, this.clyde.status);
    }

    // ----------
    // Game Stuff
    // ----------

    // Resume timer
    resumeTimer() {
        this.timer.resume();
    }

    // Reset coordinates
    resetCoordinates() {
        // Pacman coordinates
        this.pacmon.x = 13.5;
        this.pacmon.y = 23.0;
        this.pacmon.vx = this.pacmon.speed;
        this.pacmon.vy = 0;

        // Blinky's coordinates
        this.blinky.x = 13.5;
        this.blinky.y = 11;
        this.blinky.vx = this.ghost_speed;
        this.blinky.vy = 0;
        this.blinky.status = 1;

        // Pinky's coordinates
        this.pinky.x = 13.5;
        this.pinky.y = 14
        this.pinky.vx = 0;
        this.pinky.vy = 0;
        this.pinky.status = 1;

        // Inky's coordinates
        this.inky.x = 11.5;
        this.inky.y = 14;
        this.inky.vx = this.ghost_speed;
        this.inky.vy = 0;
        this.inky.status = 1;
        this.inky.flag = 0;

        // Inky's coordinates
        this.clyde.x = 15.5;
        this.clyde.y = 14;
        this.clyde.vx = this.ghost_speed;
        this.clyde.vy = 0;
        this.clyde.status = 1;
        this.clyde.flag = 0;
    }

    loseLife() {
        // Stop timer
        this.timer.pause();

        // Reset everyone's coordinates
        this.resetCoordinates();

        // Lose life
        this.pacmon.status = 0;

        return this.map;
    }

    nextLevel() {
        // Stop timer
        this.timer.delete();

        // Initialize maps
        this.map = this.createBoard();

        // Initialize variables
        this.total_dots = 244;
        this.level += 1;

        // Update level based variables
        if (this.level == 2) { 
            this.pacmon.speed = (1 / 6) * 0.9;
            this.fright_speed = (1 / 6) * 0.95;
            this.bonus_points = 300;

            this.ghost_speed = (1 / 6) * 0.85;
            this.fright_ghost_speed = (1 / 6) * 0.55;
            this.tunnel_speed = (1 / 6) * 0.45;

            this.elroy1_speed = (1 / 6) * 0.9;
            this.elroy1_dots = 30;
            this.elroy2_speed = (1 / 6) * 0.95;
            this.elroy2_dots = 15;
        } else if (this.level == 3) {
            this.bonus_points = 500;

            this.elroy1_dots = 40;
            this.elroy2_dots = 20;
        } else if (this.level == 5) {
            this.pacmon.speed = (1 / 6);
            this.fright_speed = (1 / 6);
            this.bonus_points = 700;

            this.ghost_speed = (1 / 6) * 0.95;
            this.fright_ghost_speed = (1 / 6) * 0.65;
            this.tunnel_speed = (1 / 6) * 0.5;

            this.elroy1_speed = (1 / 6);
            this.elroy2_speed = (1 / 6) * 1.05;
        } else if (this.level == 6) {
            this.elroy1_dots = 50;
            this.elroy2_dots = 25;
        } else if (this.level == 7) {
            this.bonus_points = 100;
        } else if (this.level == 9) {
            this.bonus_points = 2000;

            this.elroy1_dots = 60;
            this.elroy2_dots = 30;
        } else if (this.level == 11) {
            this.bonus_points = 3000;
        } else if (this.level == 12) {
            this.elroy1_dots = 80;
            this.elroy2_dots = 40;
        } else if (this.level == 13) {
            this.bonus_points = 5000;
        } else if (this.level == 15) {
            this.elroy1_dots = 100;
            this.elroy2_dots = 50;
        }else if (this.level == 19) {
            this.elroy1_dots = 120;
            this.elroy2_dots = 60;
        } else if (this.level > 20) {
            this.pacmon.speed = (1 / 6) * 0.9;

            this.ghost_speed = (1 / 6) * 0.95;
            this.tunnel_speed = (1 / 6) * 0.5;
        }

        // Resets everyone's coordinates
        this.resetCoordinates();

        return this.map;
    }
}